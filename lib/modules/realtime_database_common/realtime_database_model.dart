class RealtimeDatabaseModel {
  final String ref;
  final Map<String, dynamic> content;

  const RealtimeDatabaseModel({required this.ref, required this.content});

  factory RealtimeDatabaseModel.fromJson(Map<String, dynamic> data) {
    final ref = data['ref'] as String;
    final content = Map<String, dynamic>.from(data['content'] as dynamic);
    return RealtimeDatabaseModel(ref: ref, content: content);
  }

  Map<String, dynamic> toJson() {
    return {
      'ref': ref,
      'content': Map<String, dynamic>.of(content),
    };
  }
}

class RawRealtimeDatabaseModel {
  final String ref;
  final dynamic content;

  const RawRealtimeDatabaseModel({required this.ref, required this.content});

  factory RawRealtimeDatabaseModel.fromJson(Map<String, dynamic> data) {
    final ref = data['ref'] as String;
    final content = data['content'] as dynamic;
    return RawRealtimeDatabaseModel(ref: ref, content: content);
  }

  Map<String, dynamic> toJson() {
    return {
      'ref': ref,
      'content': content,
    };
  }
}
