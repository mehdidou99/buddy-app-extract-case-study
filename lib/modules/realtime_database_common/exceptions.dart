class RealtimeDatabaseError implements Exception {
  RealtimeDatabaseError([String message = '']) : _message = message;

  @override
  String toString() {
    return _message;
  }

  final String _message;
}

class RealtimeDatabaseInvalidReference extends RealtimeDatabaseError {}
