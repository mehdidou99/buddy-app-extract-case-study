import 'realtime_database_model.dart';
import 'exceptions.dart';
import 'realtime_database_client.dart';

class RealtimeDatabaseAPI {
  final String? url;

  const RealtimeDatabaseAPI({this.url});
}

class RealtimeDatabaseRepository {
  final RealtimeDatabaseAPI api;
  final RealtimeDatabaseClient client;

  const RealtimeDatabaseRepository({required this.api, required this.client});

  Future<RealtimeDatabaseModel> get(String ref) async {
    final rawContent = await _getRawContent(ref);
    final content = Map<String, dynamic>.from(rawContent);
    return RealtimeDatabaseModel(ref: ref, content: content);
  }

  Future<bool> exists(String ref) async {
    final uri = RealtimeDatabaseUri(url: api.url, ref: ref);
    final response = await client.get(uri);
    return response.body!.exists;
  }

  Future<List<RawRealtimeDatabaseModel>> getList(String ref) async {
    final rawContent = await _getRawContent(ref);
    final content = List<dynamic>.from(rawContent);
    return content
        .map((e) => RawRealtimeDatabaseModel(ref: ref, content: e))
        .toList();
  }

  Future<dynamic> _getRawContent(String ref) async {
    final uri = RealtimeDatabaseUri(url: api.url, ref: ref);
    final response = await client.get(uri);
    if (response.status == RealtimeDatabaseResponseStatus.ok) {
      final rawContent = response.body!.value as dynamic;
      if (rawContent == null) {
        throw RealtimeDatabaseInvalidReference();
      }
      return rawContent;
    } else {
      throw RealtimeDatabaseError(
          "Error while retrieving ref '$ref': ${response.errorMessage}");
    }
  }

  Future<void> set(RealtimeDatabaseModel value) async {
    final uri = RealtimeDatabaseUri(url: api.url, ref: value.ref);
    final response = await client.set(uri, body: value.content);
    if (response.status == RealtimeDatabaseResponseStatus.error) {
      throw RealtimeDatabaseError(
          "Error while sending ref '${value.ref}': ${response.errorMessage}");
    }
  }

  Future<void> setValue(String ref, dynamic value) async {
    final uri = RealtimeDatabaseUri(url: api.url, ref: ref);
    final response = await client.set(uri, body: value);
    if (response.status == RealtimeDatabaseResponseStatus.error) {
      throw RealtimeDatabaseError(
          "Error while sending ref '${value.ref}': ${response.errorMessage}");
    }
  }

  Future<void> delete(String ref) async {
    final uri = RealtimeDatabaseUri(url: api.url, ref: ref);
    final response = await client.delete(uri);
    if (response.status == RealtimeDatabaseResponseStatus.error) {
      throw RealtimeDatabaseError(
          "Error while deleting ref '$ref': ${response.errorMessage}");
    }
  }
}
