import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class RealtimeDatabaseUri {
  final String? url;
  final String ref;

  const RealtimeDatabaseUri({this.url, required this.ref});
}

enum RealtimeDatabaseResponseStatus { ok, error }

class RealtimeDatabaseResponse {
  final RealtimeDatabaseResponseStatus status;
  final DataSnapshot? body;
  final String? errorMessage;

  const RealtimeDatabaseResponse.ok({required this.body})
      : status = RealtimeDatabaseResponseStatus.ok,
        errorMessage = null;

  const RealtimeDatabaseResponse.error({required this.errorMessage})
      : status = RealtimeDatabaseResponseStatus.error,
        body = null;
}

class RealtimeDatabaseClient {
  const RealtimeDatabaseClient();

  Future<RealtimeDatabaseResponse> get(RealtimeDatabaseUri uri) async {
    final instance = _databaseInstance(uri.url);
    final body = await instance.ref(uri.ref).get();

    return RealtimeDatabaseResponse.ok(body: body);
  }

  Future<RealtimeDatabaseResponse> set(RealtimeDatabaseUri uri,
      {Object? body}) async {
    final instance = _databaseInstance(uri.url);
    await instance.ref(uri.ref).set(body);
    return const RealtimeDatabaseResponse.ok(body: null);
  }

  Future<RealtimeDatabaseResponse> delete(RealtimeDatabaseUri uri) async {
    final instance = _databaseInstance(uri.url);
    await instance.ref(uri.ref).remove();
    return const RealtimeDatabaseResponse.ok(body: null);
  }

  static FirebaseDatabase _databaseInstance(String? dataBaseUrl) {
    if (dataBaseUrl != null) {
      return FirebaseDatabase.instanceFor(
          app: Firebase.app(), databaseURL: dataBaseUrl);
    } else {
      return FirebaseDatabase.instance;
    }
  }
}
