import 'package:buddy/adapter/home/in/bloc/mobile_home_nav/mobile_home_nav_cubit.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuddyBackButton extends StatelessWidget {
  final void Function()? onPressed;
  final double? iconSize;
  const BuddyBackButton({Key? key, this.onPressed, this.iconSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
        onPressed: onPressed,
        iconSize: iconSize,
        icon: const Icon(Icons.arrow_back_ios_new_rounded));
  }
}

class BackButtonOverlay extends StatelessWidget {
  final Widget child;
  const BackButtonOverlay({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        Align(
          alignment: Alignment.topLeft,
          child: BuddyBackButton(
            onPressed: () {
              context.read<MobileHomeNavCubit>().goBack();
            },
          ),
        ),
      ],
    );
  }
}
