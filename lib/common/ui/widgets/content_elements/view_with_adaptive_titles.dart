import 'package:buddy/common/ui/widgets/content_elements/buddy_title.dart';
import 'package:flutter/material.dart';

class BuddyTitleTextContainer extends StatelessWidget {
  final Widget child;
  const BuddyTitleTextContainer({Key? key, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}

class ViewWithAdaptiveTitles extends StatelessWidget {
  final List<Widget> children;
  final int longestTitleLength;
  final double titlePadding;

  const ViewWithAdaptiveTitles({
    Key? key,
    required this.children,
    required this.longestTitleLength,
    this.titlePadding = 0.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: ((context, constraints) {
      const balooDa2AspectRatio = 3.3;
      final maxWidth = constraints.maxWidth - 2 * titlePadding;
      final maxTitleHeight =
          maxWidth / longestTitleLength * balooDa2AspectRatio;
      return ListView(
          children: children
              .map((child) => _mustBeConstrained(child)
                  ? ConstrainedBox(
                      constraints: BoxConstraints(maxHeight: maxTitleHeight),
                      child: child,
                    )
                  : child)
              .toList());
    }));
  }

  static bool _mustBeConstrained(Widget child) {
    return child is BuddyTitleText || child is BuddyTitleTextContainer;
  }
}
