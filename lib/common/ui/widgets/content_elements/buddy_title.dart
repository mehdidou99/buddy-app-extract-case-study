import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BuddyTitleSpan {
  final String text;
  final bool isEmph;
  const BuddyTitleSpan({required this.text, required this.isEmph});
  const BuddyTitleSpan.normal(String text) : this(text: text, isEmph: false);
  const BuddyTitleSpan.emph(String text) : this(text: text, isEmph: true);

  int get length => text.length;
}

class BuddyTitle {
  final List<BuddyTitleSpan> spans;

  const BuddyTitle(this.spans);

  int get length => spans.fold(0, (sum, buddySpan) => sum + buddySpan.length);
}

class BuddyTitleText extends StatelessWidget {
  static const defaultTitlePadding = 30.0;

  final BuddyTitle title;
  final double titlePadding;
  const BuddyTitleText(
      {Key? key, required this.title, this.titlePadding = defaultTitlePadding})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: titlePadding),
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: RichText(
            text: TextSpan(
                style: _textStyle,
                children: title.spans
                    .map((buddySpan) => TextSpan(
                          text: buddySpan.text,
                          style: buddySpan.isEmph
                              ? _textStyle.copyWith(
                                  color: const Color(0xFFFF5C5C),
                                  fontSize: _fontSize,
                                )
                              : _textStyle.copyWith(
                                  color: Colors.black,
                                  fontSize: _fontSize,
                                ),
                        ))
                    .toList())),
      ),
    );
  }

  static final _textStyle = GoogleFonts.balooDa2(
    fontWeight: FontWeight.bold,
    fontSize: _fontSize,
    color: Colors.black,
  );

  static const _fontSize = 28.0;
}
