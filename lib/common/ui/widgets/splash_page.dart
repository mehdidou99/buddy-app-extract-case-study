import 'package:buddy/common/ui/navigation/buddy_page.dart';
import 'package:flutter/material.dart';

class SplashPage extends BuddyPage {
  final String? process;

  const SplashPage({this.process}) : super(key: const ValueKey('SplashPage'));

  @override
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (context) => SplashScreen(process: process),
    );
  }
}

class SplashScreen extends StatelessWidget {
  final String? process;
  const SplashScreen({Key? key, this.process}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SplashContent(process: process),
    );
  }
}

class SplashContent extends StatelessWidget {
  final String? process;
  const SplashContent({Key? key, this.process}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Image(
              image: AssetImage('assets/logo_buddy_fond_transparent.png')),
          if (process != null) Text(process!),
        ],
      ),
    );
  }
}
