import 'package:flutter/material.dart';

abstract class BuddyPage extends Page {
  const BuddyPage({
    LocalKey? key,
    String? name,
    Object? arguments,
    String? restorationId,
  }) : super(
            key: key,
            name: name,
            arguments: arguments,
            restorationId: restorationId);
}

abstract class StandardBuddyPage extends BuddyPage {
  const StandardBuddyPage({required LocalKey key}) : super(key: key);

  @override
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (context) => buildContent(context),
    );
  }

  Widget buildContent(BuildContext context);
}
