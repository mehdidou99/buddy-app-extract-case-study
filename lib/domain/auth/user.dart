import 'package:equatable/equatable.dart';

/// Represents a user's minimal informations, as provided at signup.
class UserBasicInfos extends Equatable {
  /// The user's ID.
  final String uid;

  /// The user's email.
  final String email;

  /// Initializes a new instance of the [UserBasicInfos] class.
  const UserBasicInfos({
    required this.uid,
    required this.email,
  });

  @override
  List<Object?> get props => [uid, email];
}
