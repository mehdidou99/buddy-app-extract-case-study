import 'package:buddy/application/auth/port/in/command_validator.dart';
import 'package:buddy/application/auth/port/in/get_current_user_usecase.dart';
import 'package:buddy/application/auth/port/in/load_user_usecase.dart';
import 'package:buddy/application/auth/port/in/login_usecase.dart';
import 'package:buddy/application/auth/port/in/logout_usecase.dart';
import 'package:buddy/application/auth/port/in/signup_usecase.dart';
import 'package:buddy/application/auth/port/out/load_user_port.dart';
import 'package:buddy/application/auth/port/out/login_port.dart';
import 'package:buddy/application/auth/port/out/logout_port.dart';
import 'package:buddy/application/auth/port/out/signup_port.dart';
import 'package:buddy/domain/auth/user.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@singleton
class AuthService
    implements
        LoadUserUseCase,
        LoginUseCase,
        LogoutUseCase,
        SignupUseCase,
        GetCurrentUserUseCase {
  final LoadUserPort loadUserPort;
  final LoginPort loginPort;
  final LogoutPort logoutPort;
  final SignupPort signupPort;

  final BehaviorSubject<UserBasicInfos?> _streamController =
      BehaviorSubject.seeded(null);

  /// Initializes a new instance of the [AuthService] class with dependencies.
  AuthService({
    required this.loadUserPort,
    required this.loginPort,
    required this.logoutPort,
    required this.signupPort,
  }) {
    loadUserPort.loadUser().listen((userBasicInfos) {
      _streamController.add(userBasicInfos);
    });
  }

  @override
  Stream<UserBasicInfos?> loadUser() {
    return loadUserPort.loadUser().asBroadcastStream();
  }

  @override
  Future<UserBasicInfos> login(LoginCommand loginCommand) {
    if (!loginCommand.email.isValidEmail) {
      throw InvalidLoginEmailError(loginCommand.email);
    }
    return loginPort.login(loginCommand.email, loginCommand.password);
  }

  @override
  Future<void> logout() {
    return logoutPort.logout();
  }

  @override
  Future<UserBasicInfos> signup(SignupCommand command) async {
    if (!command.email.isValidEmail) {
      throw InvalidSignupEmailError(command.email);
    }
    if (!command.password.isValidPassword) {
      throw InvalidSignupPasswordError(command.password);
    }
    final uid = await signupPort.signup(command.email, command.password);
    return UserBasicInfos(uid: uid, email: command.email);
  }

  @override
  UserBasicInfos? getCurrentUser() {
    return _streamController.value;
  }
}
