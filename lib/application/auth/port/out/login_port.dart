import 'package:buddy/domain/auth/user.dart';

/// Interface for login ports.
abstract class LoginPort {
  Future<UserBasicInfos> login(String email, String password);
}

class LoginError implements Exception {
  final String _message;
  const LoginError([this._message = "Login error"]);

  @override
  String toString() {
    return _message;
  }
}

/// Exception to throw when the user does not exist.
class UserNotFoundError extends LoginError {
  /// The email of the user that does not exist.
  final String email;

  /// Initializes a new instance of the [UserNotFoundError] class.
  /// [email] The email of the user that does not exist.
  /// [message] The message of the error.
  const UserNotFoundError(this.email, [String message = '']) : super(message);

  @override
  String toString() {
    return 'UserNotFoundError: ${super.toString()} ($email)';
  }
}

/// Exception to throw when the password is incorrect.
class WrongPasswordError extends LoginError {
  /// The email of the user that tried to log in.
  final String email;

  /// The password that is incorrect.
  final String password;

  /// Initializes a new instance of the [WrongPasswordError] class.
  /// [email] The email of the user that tried to log in.
  /// [password] The password that is incorrect.
  /// [message] The message of the error.
  const WrongPasswordError(this.email, this.password, [String message = ''])
      : super(message);

  @override
  String toString() {
    return 'WrongPasswordError: ${super.toString()} ($email, $password)';
  }
}
