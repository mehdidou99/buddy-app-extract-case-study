/// Interface for signup ports.
abstract class SignupPort {
  /// Signs up and returns uid.
  Future<String> signup(String email, String password);
}

/// Exception to raise if the email is already used by an account.
class EmailAlreadyUsedError implements Exception {
  /// The email that is already used.
  final String email;
  final String _message;
  const EmailAlreadyUsedError(this.email,
      [this._message = "Email already used"]);

  @override
  String toString() {
    return 'EmailAlreadyUsedError: $_message ($email)';
  }
}
