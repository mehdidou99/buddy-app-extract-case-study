import 'package:buddy/domain/auth/user.dart';

/// Interface for load user ports.
abstract class LoadUserPort {
  Stream<UserBasicInfos?> loadUser();
}
