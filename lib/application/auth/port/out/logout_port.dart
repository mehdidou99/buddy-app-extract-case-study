/// Interface for logout ports.
abstract class LogoutPort {
  Future<void> logout();
}
