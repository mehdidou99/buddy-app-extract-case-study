import 'package:buddy/domain/auth/user.dart';

/// Interface for get current user use cases.
abstract class GetCurrentUserUseCase {
  UserBasicInfos? getCurrentUser();
}
