import 'package:buddy/domain/auth/user.dart';

/// The necessary informations for a user to log in.
class LoginCommand {
  /// The user's email.
  final String email;

  /// The user's password.
  final String password;

  /// Initializes a new instance of the [LoginCommand] class.
  /// [email] The user's email.
  /// [password] The user's password.
  const LoginCommand({
    required this.email,
    required this.password,
  });
}

/// Interface for login use cases.
abstract class LoginUseCase {
  Future<UserBasicInfos> login(LoginCommand command);
}

/// Exception to throw if the login command is invalid.
class InvalidLoginCommandError implements Exception {
  /// The message of the error.
  final String _message;
  const InvalidLoginCommandError([this._message = "Invalid login command."]);

  @override
  String toString() {
    return _message;
  }
}

/// Exception to throw if the email is invalid.
class InvalidLoginEmailError extends InvalidLoginCommandError {
  /// The invalid email.
  final String email;
  const InvalidLoginEmailError(this.email,
      [String message = "Invalid login email."])
      : super(message);
}
