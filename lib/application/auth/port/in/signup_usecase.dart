import 'package:buddy/domain/auth/user.dart';

/// The necessary informations for a user to sign up.
class SignupCommand {
  /// The user's basic infos (mail, first name, last name).
  final String email;

  /// The user's password.
  final String password;

  /// Initializes a new instance of the [SignupCommand] class.
  /// [userBasicInfos] The user's basic infos (mail, first name, last name).
  /// [password] The user's password.
  const SignupCommand({
    required this.email,
    required this.password,
  });
}

/// Interface for signup use cases.
abstract class SignupUseCase {
  Future<UserBasicInfos> signup(SignupCommand command);
}

/// Exception to throw if the signup command is invalid.
class InvalidSignupCommandError implements Exception {
  /// The message of the error.
  final String _message;

  /// Initializes a new instance of the [InvalidSignupCommandError] class.
  /// [message] The message of the error.
  const InvalidSignupCommandError([this._message = "Invalid signup command"]);

  @override
  String toString() {
    return _message;
  }
}

/// Exception to throw if the email is not a valid email.
class InvalidSignupEmailError extends InvalidSignupCommandError {
  /// The email that is not valid.
  final String email;

  /// Initializes a new instance of the [InvalidSignupEmailError] class.
  /// [email] The email that is not valid.
  /// [message] The message of the error.
  const InvalidSignupEmailError(this.email, [String message = "Invalid email."])
      : super(message);
}

class InvalidSignupPasswordError extends InvalidSignupCommandError {
  /// The password that is not valid.
  final String password;

  /// Initializes a new instance of the [InvalidSignupPasswordError] class.
  /// [password] The email that is not valid.
  /// [message] The message of the error.
  const InvalidSignupPasswordError(this.password,
      [String message = "Invalid password."])
      : super(message);
}
