/// Interface for logout use cases.
abstract class LogoutUseCase {
  Future<void> logout();
}
