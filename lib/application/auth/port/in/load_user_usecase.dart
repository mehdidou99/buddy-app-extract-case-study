import 'package:buddy/domain/auth/user.dart';

/// Interface for load user use cases.
abstract class LoadUserUseCase {
  Stream<UserBasicInfos?> loadUser();
}
