import 'package:buddy/application/auth/port/out/load_user_port.dart';
import 'package:buddy/application/auth/port/out/login_port.dart';
import 'package:buddy/application/auth/port/out/logout_port.dart';
import 'package:buddy/application/auth/port/out/signup_port.dart';
import 'package:buddy/domain/auth/user.dart';
import 'package:buddy/modules/realtime_database_common/realtime_database_common.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

@singleton
class AuthAdapter implements LoadUserPort, LoginPort, LogoutPort, SignupPort {
  final firebaseAuth = FirebaseAuth.instance;
  final RealtimeDatabaseRepository realtimeDatabaseRepository;

  AuthAdapter({required this.realtimeDatabaseRepository});

  @override
  Stream<UserBasicInfos?> loadUser() {
    return firebaseAuth.userChanges().asyncMap((currentUser) async {
      return currentUser == null
          ? null
          : UserBasicInfos(uid: currentUser.uid, email: currentUser.email!);
    });
  }

  @override
  Future<UserBasicInfos> login(String email, String password) async {
    final userCredentials = await _getUserCredentials(email, password);
    if (userCredentials.user == null) {
      final signInMethods =
          await firebaseAuth.fetchSignInMethodsForEmail(email);
      if (signInMethods.contains('password')) {
        throw WrongPasswordError(email, password);
      } else {
        throw UserNotFoundError(email);
      }
    }

    final firebaseUser = userCredentials.user!;
    return UserBasicInfos(uid: firebaseUser.uid, email: email);
  }

  @override
  Future<void> logout() => FirebaseAuth.instance.signOut();

  @override
  Future<String> signup(String email, String password) async {
    final userCredentials = await _createUser(email, password);

    if (userCredentials.user == null) {
      throw EmailAlreadyUsedError(email);
    }

    final firebaseUser = userCredentials.user!;
    return firebaseUser.uid;
  }

  Future<UserCredential> _getUserCredentials(
      String email, String password) async {
    try {
      return await firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw e.message != null
            ? UserNotFoundError(email, e.message!)
            : UserNotFoundError(email);
      } else if (e.code == 'wrong-password') {
        throw e.message != null
            ? WrongPasswordError(email, password, e.message!)
            : WrongPasswordError(email, password);
      } else {
        rethrow;
      }
    }
  }

  Future<UserCredential> _createUser(String email, String password) async {
    try {
      return await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'email-already-in-use') {
        throw e.message != null
            ? EmailAlreadyUsedError(email, e.message!)
            : EmailAlreadyUsedError(email);
      } else {
        rethrow;
      }
    }
  }
}
