import 'package:buddy/application/auth/port/out/login_port.dart';
import 'package:buddy/application/auth/port/out/signup_port.dart';
import 'package:buddy/domain/auth/user.dart';
import 'package:equatable/equatable.dart';

/// The state of the authentication bloc.
abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object?> get props => [];
}

/// The state when the authentication status is still unknown.
class UnknownAuthState extends AuthState {
  const UnknownAuthState();
}

/// The state when the user is logged out.
class LoggedOutState extends AuthState {
  const LoggedOutState();
}

/// The state when the user is logged in.
class LoggedInState extends AuthState {
  /// The logged in user's basic infos.
  final UserBasicInfos userBasicInfos;
  const LoggedInState({required this.userBasicInfos});

  @override
  List<Object?> get props => super.props..addAll([userBasicInfos]);
}

enum ErrorSource { email, password }

class ErrorState extends AuthState {
  final ErrorSource source;
  final String hint;
  final String? suggestion;
  const ErrorState({required this.source, required this.hint, this.suggestion});

  const ErrorState.emailAlreadyUsed(EmailAlreadyUsedError error)
      : this(
          source: ErrorSource.email,
          hint: 'Email déjà utilisé',
          suggestion: 'Souhaites-tu te connecter ?',
        );
  const ErrorState.userNotFound(UserNotFoundError error)
      : this(
          source: ErrorSource.email,
          hint: "Aucun compte ne correspond à cet email",
          suggestion: "Souhaites-tu t'inscrire ?",
        );
  const ErrorState.wrongPassword(WrongPasswordError error)
      : this(
          source: ErrorSource.password,
          hint: "La classique... Mauvais mot de passe",
        );

  @override
  List<Object?> get props => super.props..addAll([source, hint]);
}
