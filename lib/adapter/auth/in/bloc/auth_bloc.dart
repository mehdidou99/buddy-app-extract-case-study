import 'dart:async';

import 'package:buddy/adapter/auth/in/bloc/auth_events.dart';
import 'package:buddy/adapter/auth/in/bloc/auth_states.dart';
import 'package:buddy/application/auth/port/in/load_user_usecase.dart';
import 'package:buddy/application/auth/port/in/login_usecase.dart';
import 'package:buddy/application/auth/port/in/logout_usecase.dart';
import 'package:buddy/application/auth/port/in/signup_usecase.dart';
import 'package:buddy/application/auth/port/out/login_port.dart';
import 'package:buddy/application/auth/port/out/signup_port.dart';
import 'package:buddy/domain/auth/user.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final LoadUserUseCase loadUserUseCase;
  final LoginUseCase loginUseCase;
  final LogoutUseCase logoutUseCase;
  final SignupUseCase signupUseCase;
  AuthBloc(
      {required this.loadUserUseCase,
      required this.loginUseCase,
      required this.logoutUseCase,
      required this.signupUseCase})
      : super(const UnknownAuthState()) {
    on<UserChangedEvent>((event, emit) async {
      if (event.userBasicInfos != null) {
        emit(LoggedInState(userBasicInfos: event.userBasicInfos!));
      } else {
        emit(const LoggedOutState());
      }
    });
    on<LoginEvent>((event, emit) async {
      try {
        final userBasicInfos = await loginUseCase.login(event.loginCommand);
        emit(LoggedInState(userBasicInfos: userBasicInfos));
      } on UserNotFoundError catch (e) {
        emit(ErrorState.userNotFound(e));
      } on WrongPasswordError catch (e) {
        emit(ErrorState.wrongPassword(e));
      }
    });
    on<LogoutEvent>((event, emit) async {
      await logoutUseCase.logout();
      emit(const LoggedOutState());
    });
    on<SignupEvent>((event, emit) async {
      try {
        final userBasicInfos = await signupUseCase.signup(event.signupCommand);
        emit(LoggedInState(userBasicInfos: userBasicInfos));
      } on EmailAlreadyUsedError catch (e) {
        emit(ErrorState.emailAlreadyUsed(e));
      }
    });

    _subscribeToUserChanges();
  }

  @override
  Future<void> close() async {
    await _loadUserSubscription?.cancel();
    return super.close();
  }

  void _subscribeToUserChanges() {
    _loadUserSubscription = loadUserUseCase.loadUser().listen(
        (userBasicInfos) =>
            add(UserChangedEvent(userBasicInfos: userBasicInfos)));
  }

  StreamSubscription<UserBasicInfos?>? _loadUserSubscription;
}
