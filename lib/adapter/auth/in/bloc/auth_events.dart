import 'package:buddy/application/auth/port/in/login_usecase.dart';
import 'package:buddy/application/auth/port/in/signup_usecase.dart';
import 'package:buddy/domain/auth/user.dart';

/// An event of the authentication bloc.
abstract class AuthEvent {
  const AuthEvent();
}

/// Triggered when the user changes.
class UserChangedEvent extends AuthEvent {
  /// The basic infos of the new user if there is one, else null.
  final UserBasicInfos? userBasicInfos;
  const UserChangedEvent({required this.userBasicInfos});
}

/// Triggered when the user logs in.
class LoginEvent extends AuthEvent {
  /// The login command.
  final LoginCommand loginCommand;
  const LoginEvent(this.loginCommand);
}

/// Triggered when the user logs out.
class LogoutEvent extends AuthEvent {
  const LogoutEvent();
}

/// Triggered when the user signs up.
class SignupEvent extends AuthEvent {
  /// The signup command.
  final SignupCommand signupCommand;
  const SignupEvent(this.signupCommand);
}
