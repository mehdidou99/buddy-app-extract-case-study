import 'package:buddy/adapter/auth/in/bloc/auth_bloc.dart';
import 'package:buddy/adapter/auth/in/bloc/auth_events.dart';
import 'package:buddy/adapter/auth/in/ui/widgets/login_or_signup_widgets.dart';
import 'package:buddy/application/auth/port/in/command_validator.dart';
import 'package:buddy/application/auth/port/in/signup_usecase.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignupForm extends StatefulWidget {
  const SignupForm({Key? key}) : super(key: key);

  @override
  State<SignupForm> createState() => _SignupFormState();
}

class _SignupFormState extends State<SignupForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          LoginOrSignupTextField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            autocorrect: false,
            validator: (email) {
              if (email != null && email.isValidEmail) {
                return null;
              }
              return "Email invalide";
            },
            hintText: "Adresse email",
            icon: Icons.mail_outline,
          ),
          const SizedBox(height: 16.0),
          LoginOrSignupTextField(
            controller: _passwordController,
            obscureText: true,
            validator: (password) {
              if (password != null && password.isValidPassword) {
                return null;
              }
              return "Le mot de passe doit faire au moins 8 caractères";
            },
            hintText: "Mot de passe",
            icon: Icons.lock_outline,
          ),
          const LoginOrSignupErrorHint(),
          LoginOrSignupButton(
            onPressed: _onSignupButtonPressed,
            text: 'Créer un compte',
          ),
        ],
      ),
    );
  }

  void _onSignupButtonPressed() {
    final isValid = _formKey.currentState!.validate();

    if (isValid) {
      final email = _emailController.text;
      context.read<AuthBloc>().add(SignupEvent(
          SignupCommand(email: email, password: _passwordController.text)));
    }
  }
}
