import 'package:buddy/adapter/auth/in/bloc/auth_bloc.dart';
import 'package:buddy/adapter/auth/in/bloc/auth_events.dart';
import 'package:buddy/adapter/auth/in/ui/widgets/login_or_signup_widgets.dart';
import 'package:buddy/application/auth/port/in/login_usecase.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:buddy/application/auth/port/in/command_validator.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          LoginOrSignupTextField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            autocorrect: false,
            validator: (email) {
              if (email != null && email.isValidEmail) {
                return null;
              }
              return "Email invalide";
            },
            hintText: "Adresse email",
            icon: Icons.mail_outline,
          ),
          const SizedBox(height: 16.0),
          LoginOrSignupTextField(
            controller: _passwordController,
            obscureText: true,
            validator: (password) {
              if (password != null && password.isValidPassword) {
                return null;
              }
              return "Le mot de passe doit faire au moins 8 caractères";
            },
            hintText: "Mot de passe",
            icon: Icons.lock_outline,
          ),
          const LoginOrSignupErrorHint(),
          LoginOrSignupButton(
            onPressed: _onLoginButtonPressed,
            text: 'Se connecter',
          ),
        ],
      ),
    );
  }

  void _onLoginButtonPressed() {
    final isValid = _formKey.currentState!.validate();

    if (isValid) {
      context.read<AuthBloc>().add(LoginEvent(LoginCommand(
          email: _emailController.text, password: _passwordController.text)));
    }
  }
}
