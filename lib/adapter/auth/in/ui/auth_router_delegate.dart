import 'package:buddy/adapter/auth/in/bloc/auth_bloc.dart';
import 'package:buddy/adapter/auth/in/bloc/auth_states.dart';
import 'package:buddy/adapter/auth/in/ui/login_or_signup_screen.dart';
import 'package:buddy/common/ui/widgets/splash_page.dart';
import 'package:buddy/domain/auth/user.dart';
import 'package:buddy/common/ui/navigation/buddy_page.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthRouterDelegate extends RouterDelegate
    with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  final Widget Function(
          Key navigatorKey, BuildContext context, UserBasicInfos infos)
      loggedInBuilder;
  final GlobalKey<NavigatorState> _navigatorKey;
  @override
  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  AuthRouterDelegate(
      {required this.loggedInBuilder, GlobalKey<NavigatorState>? navigatorKey})
      : _navigatorKey = navigatorKey ?? GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, authState) {
      if (authState is LoggedInState) {
        return loggedInBuilder(navigatorKey, context, authState.userBasicInfos);
      } else {
        return Navigator(
          key: navigatorKey,
          pages: _buildStack(authState),
          onPopPage: (route, result) {
            // if (!route.didPop(result)) return false;
            return false;
          },
        );
      }
    });
  }

  List<BuddyPage> _buildStack(AuthState authState) {
    if (authState is UnknownAuthState) {
      return [const SplashPage()];
    } else if (authState is LoggedOutState || authState is ErrorState) {
      return [const LoginOrSignupPage()];
    } else {
      throw UnimplementedError();
    }
  }

  @override
  Future<void> setNewRoutePath(configuration) async {}
}
