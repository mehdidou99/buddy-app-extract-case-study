import 'package:buddy/adapter/auth/in/ui/login_form.dart';
import 'package:buddy/adapter/auth/in/ui/signup_form.dart';
import 'package:buddy/adapter/auth/in/ui/widgets/login_or_signup_widgets.dart';
import 'package:buddy/common/ui/navigation/buddy_page.dart';
import 'package:buddy/common/old/widgets/visual_elements/app_bar_padder.dart';
import 'package:buddy/common/old/widgets/visual_elements/buddy_background_container.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginOrSignupPage extends BuddyPage {
  const LoginOrSignupPage() : super(key: const ValueKey('LoginOrSignupPage'));
  @override
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (context) => const LoginOrSignupScreen(),
    );
  }
}

class LoginOrSignupScreen extends StatefulWidget {
  const LoginOrSignupScreen({Key? key}) : super(key: key);

  @override
  State<LoginOrSignupScreen> createState() => _LoginOrSignupScreenState();
}

enum SigninMode { login, signup }

class _LoginOrSignupScreenState extends State<LoginOrSignupScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BuddyBackgroundContainer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const AppBarPadder(),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Spacer(flex: 3),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 80),
                    child: Image(
                      image:
                          AssetImage('assets/logo_buddy_fond_transparent.png'),
                    ),
                  ),
                  Text(
                    "La vie d'adulte, un jeu d'enfant",
                    style: GoogleFonts.balooDa2(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                    ),
                  ),
                  const Spacer(flex: 1),
                  Text(
                    "Bienvenue ☀️",
                    style: GoogleFonts.sen(
                      fontWeight: FontWeight.bold,
                      fontSize: 44,
                    ),
                  ),
                  const Spacer(flex: 1),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 45,
                    ),
                    child: Column(
                      children: [
                        _signinMode == SigninMode.login
                            ? const LoginForm()
                            : const SignupForm(),
                        ChangeActionClickableText(
                          onPressed: () {
                            setState(() {
                              _signinMode = _signinMode.toggle();
                            });
                          },
                          prefix: _signinMode == SigninMode.login
                              ? "Pas encore inscrit ? "
                              : "Déjà inscrit ? ",
                          text: _signinMode == SigninMode.login
                              ? "Ou s'inscrire"
                              : "Se connecter",
                        ),
                      ],
                    ),
                  ),
                  const Spacer(flex: 5),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  SigninMode _signinMode = SigninMode.signup;
}

extension _TogglableSigninMode on SigninMode {
  SigninMode toggle() {
    return this == SigninMode.login ? SigninMode.signup : SigninMode.login;
  }
}
