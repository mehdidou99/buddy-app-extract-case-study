import 'package:buddy/adapter/auth/in/bloc/auth_bloc.dart';
import 'package:buddy/adapter/auth/in/bloc/auth_states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

class ChangeActionClickableText extends StatelessWidget {
  final void Function()? onPressed;
  final String? prefix;
  final String text;
  const ChangeActionClickableText(
      {Key? key, required this.onPressed, required this.text, this.prefix})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.fitHeight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (prefix != null)
            Text(
              prefix!,
              style: _getTextStyle(underline: false),
            ),
          InkWell(
            onTap: onPressed,
            child: Text(
              text,
              style: _getTextStyle(underline: true),
            ),
          ),
        ],
      ),
    );
  }

  TextStyle _getTextStyle({required bool underline}) => GoogleFonts.balooDa2(
        color: const Color(0xFF4A4A4A),
        fontSize: 16,
        decoration: underline ? TextDecoration.underline : null,
      );
}

class LoginOrSignupButton extends StatelessWidget {
  final void Function()? onPressed;
  final String text;

  const LoginOrSignupButton(
      {Key? key, required this.onPressed, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        minimumSize: const Size.fromHeight(40),
        backgroundColor: Colors.black,
        foregroundColor: Colors.white,
        textStyle: GoogleFonts.balooDa2(
          color: Colors.white,
          fontSize: 14,
          fontWeight: FontWeight.w700,
        ),
      ),
      child: Text(
        text,
        softWrap: false,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}

class LoginOrSignupTextField extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType? keyboardType;
  final bool obscureText;
  final bool autocorrect;
  final IconData? icon;
  final String? hintText;
  final String? Function(String?)? validator;

  const LoginOrSignupTextField({
    Key? key,
    required this.controller,
    this.keyboardType,
    this.autocorrect = true,
    this.obscureText = false,
    this.hintText,
    this.icon,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        controller: controller,
        keyboardType: keyboardType,
        autocorrect: autocorrect,
        obscureText: obscureText,
        validator: validator,
        cursorColor: const Color(0xFFB56ADD),
        style: GoogleFonts.balooDa2(),
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          hintText: hintText,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: const BorderSide(color: Color(0xFFB56ADD), width: 2)),
          hintStyle: MaterialStateTextStyle.resolveWith((states) =>
              GoogleFonts.balooDa2(
                  color: states.isError() ? Colors.redAccent : Colors.grey)),
          suffixIcon: icon != null ? Icon(icon, color: Colors.grey) : null,
          // Does not work because of a known bug of the material library.
          suffixIconColor: MaterialStateColor.resolveWith((states) =>
              states.contains(MaterialState.focused)
                  ? const Color(0xFFB56ADD)
                  : const Color(0xFFB56ADD)),
        ));
  }
}

class LoginOrSignupErrorHint extends StatelessWidget {
  const LoginOrSignupErrorHint({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      if (state is ErrorState) {
        return SizedBox(
          height: 100.0,
          child: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(state.hint, style: _errorTextStyle),
              if (state.suggestion != null)
                Text(state.suggestion!, style: _errorTextStyle)
            ]),
          ),
        );
      } else {
        return const SizedBox(height: 100.0);
      }
    });
  }

  static final _errorTextStyle = GoogleFonts.balooDa2(color: Colors.redAccent);
}

extension _SmartMaterialState on Set<MaterialState> {
  bool isError() => contains(MaterialState.error);
}
