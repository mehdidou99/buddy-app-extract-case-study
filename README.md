# Etude de cas : exploration du code de Buddy

## Ta mission

Tu trouveras dans le dossier `lib/` un extrait du code de Buddy implémentant une fonctionnalité clé de l'application. Ton objectif est d'explorer et comprendre l'essentiel du code de cette fonctionnalité. Ce n'est pas important que tu comprennes tous les éléments de syntaxe, puisque tu ne connais pas en détails Dart et Flutter, mais le but est plutôt de bien comprendre le fonctionnement même si les détails de syntaxe t'échappent.

Je te conseille de ne pas passer plus d'une heure sur cette exercice, pas besoin d'apprendre le code par coeur non plus !

## Rendu

Ton objectif est de préparer une petite "fiche récap" qui contient :
- En une phrase, quelle est la fonctionnalité implémentée
- Une section sur comment le code est organisé :
  - Comment est structurée l'arborescence de fichiers ?
  - A quoi correspondent les différents dossiers et fichiers ?
- Une présentation des grosses briques/parties du code que tu juges les plus importantes :
  - A quoi servent-t-elles ?
  - Comment sont-elles-implémentées ? Reconnais-tu des design patterns/des manières de faire que tu connais ?
- Des remarques générales sur la qualité du code :
  - Que penses-tu de la clarté du code ?
  - Que manque-t-il selon toi en termes de qualité du code ?
  - Que penses-tu de Dart et Flutter après ce premier aperçu ?
  - Etc
- Des questions que tu te poses :
  - Quels sont les éléments que tu n'as pas compris dans le code ?
  - Quelles questions sur Dart et Flutter te poses-tu ?
  - Etc

# Interview n°2

Lors de l'éventuelle deuxième interview, on prendra un moment pour débriefer cette étude de cas :
- Je te ferai des retours sur ta fiche récap
- On discutera ensemble des remarques générales que tu as faites, et de comment les prendre en compte pour améliorer le code
- Je répondrai aux questions que tu as posées dans la fiche récap + n'importe quelle question que tu te poses sur le code/Dart et Flutter/autre.

## Bonne chance !

